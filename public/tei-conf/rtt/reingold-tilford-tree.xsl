<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei xs"
    version="2.0">
    <xsl:variable name="nb" select="//title[1]/."/>
    <xsl:template match="/">
         <xsl:result-document href="c07.html" indent="yes">
<html>
	<meta charset="utf-8" />
	<style>

		.node circle {
		  fill: #fff;
		  stroke: steelblue;
		  stroke-width: 1.5px;
		}

		.node {
		  font: 10px sans-serif;
		}

		.link {
		  fill: none;
		  stroke: #ccc;
		  stroke-width: 1.5px;
		}

		</style>
		<body>
		<script src="d3.v3.js"></script>
		<script>

		var diameter = 1200;

		var tree = d3.layout.tree()
		    .size([360, diameter / 2 - 120])
		    .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

		var diagonal = d3.svg.diagonal.radial()
		    .projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });

		var svg = d3.select("body").append("svg")
		    .attr("width", diameter)
		    .attr("height", diameter + 150)
		  .append("g")
		    .attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

		d3.json("c07.json", function(error, root) {
		  var nodes = tree.nodes(root),
		      links = tree.links(nodes);

		  var link = svg.selectAll(".link")
		      .data(links)
		    .enter().append("path")
		      .attr("class", "link")
		      .attr("d", diagonal);

		  var node = svg.selectAll(".node")
		      .data(nodes)
		    .enter().append("g")
		      .attr("class", "node")
		      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

		  node.append("circle")
		      .attr("r", 4.5);

		  node.append("text")
		      .attr("dy", ".31em")
		      .attr("text-anchor", function(d) { return d.x &lt; 180 ? "start" : "end"; })
		      .attr("transform", function(d) { return d.x &gt; 180 ? "translate(-8)" : "rotate(180)translate(8)"; })
		      .text(function(d) { return d.name; });
		});
		d3.select(self.frameElement).style("height", diameter - 150 + "px");
		</script>
		</body>
</html>
             
         </xsl:result-document>
        <xsl:result-document href="c07.json" method="text" indent="no">
            {
            "name": "C07",
            "children": [
            <xsl:apply-templates select="//tei:surface[descendant::tei:rs]" />
            ]
            }
        </xsl:result-document>
	</xsl:template>
    <xsl:template match="tei:surface[descendant::tei:rs]">
        <xsl:variable name="numberOfSurface" select="count(//tei:surface[descendant::tei:rs])" />
        <xsl:variable name="numberOfRS" select="count(descendant::tei:rs)" />
            {
                "name": "<xsl:value-of select="./@n"/>",
                "children": [
                <xsl:for-each select="./descendant::tei:rs">
                    <xsl:variable name="currentRS" select="count(preceding::rs)" />
                    {"name": "<xsl:value-of select="substring-after(./@ref, '#')"/>", "size": 1}<xsl:if test="not(position() = $numberOfRS)">,</xsl:if>
                </xsl:for-each>
                ]
                }<xsl:if test="not(position() = $numberOfSurface)">,</xsl:if>

    </xsl:template>
</xsl:stylesheet>