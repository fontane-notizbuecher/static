<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei xs" version="2.0">
    <xsl:variable name="step1" />
    <xsl:template match="/">
        <xsl:result-document href="c07.html" indent="yes">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
                    <link type="text/css" rel="stylesheet" href="style.css"/>
                    <style type="text/css">
                        path.arc{
                            cursor:move;
                            fill:#fff;
                        }
                        
                        .node{
                            font-size:12px;
                            font-family:'Ubuntu Condensed', Verdana, sans-serf, sans;
                        }
                        
                        .node:hover{
                            fill:#1f77b4;
                        }
                        
                        .link{
                            fill:none;
                            stroke:#1f77b4;
                            stroke-opacity:.4;
                            pointer-events:none;
                        }
                        
                        .link.source,
                        .link.target{
                            stroke-opacity:1;
                            stroke-width:2px;
                        }
                        
                        .node.target{
                            fill:#d62728 !important;
                        }
                        
                        .link.source{
                            stroke:#d62728;
                        }
                        
                        .node.source{
                            fill:#2ca02c;
                        }
                        
                        .link.target{
                            stroke:#2ca02c;
                        }</style>
                </head>
                <body>
                    <div style="z-index:100;position:fixed;bottom:0;font-size:18px;font-family:'Ubuntu Condensed', Verdana, sans-serf, sans;">tension: <input
                            style="position:relative;top:3px;" type="range" min="-50" max="130"
                            value="85"/></div>
                    <script type="text/javascript" src="d3.js"/>

                    <script type="text/javascript" src="d3.layout.js"/>

                    <script type="text/javascript" src="packages.js"/>

                    <script type="text/javascript" src="script.js"/>
                </body>
            </html>
        </xsl:result-document>
        
               
           <xsl:variable name="step1"><xsl:element name="C07">
                <xsl:apply-templates select="//tei:rs"/>
            </xsl:element>
           </xsl:variable>
<xsl:result-document href="c07-network.xml" method="xml" indent="yes"><xsl:value-of select="$step1"/></xsl:result-document>
        <xsl:result-document href="c07-network-experiment.json" method="text" indent="no"> [ 
            <xsl:variable name="step2">
            <xsl:for-each select="$step1//entity">
                <xsl:variable name="noE" select="count(//entity)"/>
                <xsl:variable name="noL" select="count(descendant::link)"/>
                <xsl:text>{&#x22;name&#x22;:&#x22;</xsl:text>
                <xsl:value-of select="concat(@surface, '.', @rs)"/>
                <xsl:text>&#x22;, &#x22;size&#x22;:1,&#x22;imports&#x22;:[</xsl:text>
                <xsl:for-each select="link[@surface != ../@surface]">
                    <xsl:variable name="link" select="." />
                    <xsl:text>"</xsl:text>
                    <xsl:value-of select="concat(//entity[@rs = $link]/@surface, '.' ,text())"/>
                    <xsl:text>"</xsl:text>
                    <xsl:if test="position() != $noL">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                </xsl:for-each>
                
                <xsl:text>]}</xsl:text>
                <xsl:if test="position() != $noE">
                    <xsl:text>,
            </xsl:text>
                </xsl:if>
                
            </xsl:for-each>
            </xsl:variable>
            <xsl:value-of select="replace(replace($step2, '[,]\]\}', ']}'), '[&#x22;](\d)(r|v)', '&#x22;0$1$2')" />
            ] </xsl:result-document>
    </xsl:template>


      <xsl:template match="//tei:rs">
        <xsl:variable name="current" select="@ref"/>
        <xsl:variable name="current2"
            select="(replace(substring-after(replace(@ref, 'XXX', '#unknown'), '#'), ' #', '-'))"/>
        <xsl:if test="not(@ref = preceding::tei:rs/@ref or starts-with(@ref, '#Archivar'))">
            <xsl:element name="entity"><xsl:attribute name="rs" select="$current2" />
                <xsl:attribute name="surface">
                    <xsl:choose>
                        <xsl:when
                            test="starts-with(ancestor::tei:surface/@n, 'outer') or starts-with(ancestor::tei:surface/@n, 'inner')"
                            >00</xsl:when>
                        <xsl:otherwise>
<!--                            <xsl:variable name="n">
                                <xsl:if test="ends-with(ancestor::surface/@n, 'r')">
                                    <xsl:variable name="nn" select="number(substring-before(ancestor::surface/@n, 'r'))"/>
                                    <xsl:value-of select="format-number($nn, '000')"/>
                                </xsl:if>
                                <xsl:if test="ends-with(ancestor::surface/@n, 'v')">
                                    <xsl:variable name="nn" select="number(substring-before(ancestor::surface/@n, 'v'))"/>
                                    <xsl:value-of select="format-number($nn, '000')"/>
                                </xsl:if>
                            </xsl:variable>
                            <xsl:variable name="rv">
                                <xsl:if test="ends-with(ancestor::surface/@n, 'r')">
                                    <xsl:value-of select="'r'"/>
                                </xsl:if>
                                <xsl:if test="ends-with(ancestor::surface/@n, 'v')">
                                    <xsl:value-of select="'v'"/>
                                </xsl:if>
                            </xsl:variable>
                            <xsl:value-of select="concat($n, $rv)"/>-->
                            <xsl:value-of select="ancestor::tei:surface/@n"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:for-each
                    select="//tei:rs[ancestor::tei:surface/descendant::tei:rs[@ref = $current]]"><!-- letztes attribut: contains(@ref, $current) Folge: Mehr Ergebnisse, mehr thematische Bezogenheit? : Luther, Cranach-->
                    <xsl:element name="link">
                        <xsl:attribute name="surface">
                            <xsl:choose>
                                <xsl:when
                                    test="starts-with(ancestor::tei:surface/@n, 'outer') or starts-with(ancestor::tei:surface/@n, 'inner')"
                                    >00</xsl:when>
                                <xsl:otherwise>
<!--                                    <xsl:variable name="n">
                                        <xsl:if test="ends-with(ancestor::surface/@n, 'r')">
                                            <xsl:variable name="nn" select="number(substring-before(ancestor::surface/@n, 'r'))"/>
                                            <xsl:value-of select="format-number($nn, '000')"/>
                                        </xsl:if>
                                        <xsl:if test="ends-with(ancestor::surface/@n, 'v')">
                                            <xsl:variable name="nn" select="number(substring-before(ancestor::surface/@n, 'v'))"/>
                                            <xsl:value-of select="format-number($nn, '000')"/>
                                        </xsl:if>
                                    </xsl:variable>
                                    <xsl:variable name="rv">
                                        <xsl:if test="ends-with(ancestor::surface/@n, 'r')">
                                            <xsl:value-of select="'r'"/>
                                        </xsl:if>
                                        <xsl:if test="ends-with(ancestor::surface/@n, 'v')">
                                            <xsl:value-of select="'v'"/>
                                        </xsl:if>
                                    </xsl:variable>
                                    <xsl:value-of select="concat($n, $rv)"/>-->
                                    <xsl:value-of select="ancestor::tei:surface/@n"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:value-of
                            select="(replace(substring-after(replace(@ref, 'XXX', '#unknown'), '#'), ' #', '-'))"
                        />
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
