<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wgs84_pos="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="tei xs" version="2.0">
    <xsl:template match="/">
        <kml xmlns="http://www.opengis.net/kml/2.2">
            <Folder>
                <xsl:apply-templates select="//tei:sourceDoc//tei:date"/>
            </Folder>
        </kml>
    </xsl:template>
    <xsl:template match="tei:date">
        <xsl:if test="not(./@prev)">
            <xsl:variable name="place_id">
                <xsl:choose>
                    <xsl:when test="(name(following::element()[1]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[1]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[1]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[1]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[1]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[1]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(following::element()[2]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[2]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[2]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[2]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[2]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[2]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(following::element()[3]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[3]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[3]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[3]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[3]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[3]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(following::element()[4]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[4]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[4]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[4]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[4]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[4]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(following::element()[5]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[5]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[5]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[5]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[5]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[5]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(following::element()[6]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[6]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[6]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[6]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[6]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[6]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(following::element()[7]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/following::element()[7]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/following::element()[7]/@ref,2)"/>
                    </xsl:when>
                    <xsl:when test="(name(preceding::element()[7]) eq 'rs') and (name(//tei:sourceDesc//*[@xml:id eq substring(current()/preceding::element()[7]/@ref,2)]) eq 'place')">
                        <xsl:value-of select="substring(current()/preceding::element()[7]/@ref,2)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:if test="$place_id ne ''">
                <Placemark xmlns="http://www.opengis.net/kml/2.2">
                    <name>
                        <xsl:value-of select="$place_id"/>
                    </name>
                    <address>
                        <xsl:value-of select="$place_id"/>
                    </address>
                    <xsl:choose>
                        <xsl:when test="./@when-iso">
                            <TimeStamp>
                                <when>
                                    <xsl:value-of select="./@when-iso"/>
                                </when>
                            </TimeStamp>
                        </xsl:when>
                        <xsl:otherwise>
                            <TimeSpan>
                                <begin>
                                    <xsl:value-of select="./@notBefore-iso"/>
                                </begin>
                                <end>
                                    <xsl:value-of select="./@notAfter-iso"/>
                                </end>
                            </TimeSpan>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
<!--                        <xsl:when test="//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno/@type eq 'Wikipedia'">-->
<!--                            <Point>-->
<!--                                <coordinates>-->
<!--                                    <xsl:value-of select="document(concat('http://de.wikipedia.org/wiki/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno))//span[@class eq 'longitude']"/>-->
<!--                                    <xsl:text>,</xsl:text>-->
<!--                                    <xsl:value-of select="document(concat('http://de.wikipedia.org/wiki/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno))//span[@class eq 'latitude']"/>-->
<!--                                </coordinates>-->
<!--                            </Point>-->
<!--                        </xsl:when>-->
                        <xsl:when test="//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno/@type eq 'GeoNames'">
                            <Point>
                                <coordinates>
                                    <xsl:value-of select="document(concat('http://sws.geonames.org/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno,'/about.rdf'))//wgs84_pos:long"/>
                                    <xsl:text>,</xsl:text>
                                    <xsl:value-of select="document(concat('http://sws.geonames.org/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno,'/about.rdf'))//wgs84_pos:lat"/>
                                </coordinates>
                            </Point>
                        </xsl:when>
                        <xsl:when test="//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno/@type eq 'OpenStreetMap'">
                            <MultiGeometry>
                                <Polygon>
                                    <outerBoundaryIs>
                                        <LinearRing>
                                            <coordinates>
                                                <xsl:for-each select="document(concat('http://www.openstreetmap.org/api/0.6/way/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno))//nd/@ref">
                                                    <xsl:value-of select="document(concat('http://www.openstreetmap.org/api/0.6/node/',.))//node/@lon"/>
                                                    <xsl:text>,</xsl:text>
                                                    <xsl:value-of select="document(concat('http://www.openstreetmap.org/api/0.6/node/',.))//node/@lat"/>
                                                    <xsl:text>
</xsl:text>
                                                </xsl:for-each>
                                            </coordinates>
                                        </LinearRing>
                                    </outerBoundaryIs>
                                </Polygon>
                                <Point>
                                    <coordinates>
                                        <xsl:value-of select="document(concat('http://www.openstreetmap.org/api/0.6/node/',document(concat('http://www.openstreetmap.org/api/0.6/way/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno))//nd[1]/@ref))//node/@lon"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="document(concat('http://www.openstreetmap.org/api/0.6/node/',document(concat('http://www.openstreetmap.org/api/0.6/way/',//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno))//nd[1]/@ref))//node/@lat"/>
                                    </coordinates>
                                </Point>
                            </MultiGeometry>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="//tei:sourceDesc//*[@xml:id eq $place_id]/tei:idno/@type"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    
<!--                    <Point>
                        <coordinates>
                            <xsl:value-of select="document(concat('http://nominatim.openstreetmap.org/search?format=xml&q=',$place_id))/searchresults/place[1]/@lon"/>
                            <xsl:text>,</xsl:text>
                            <xsl:value-of select="document(concat('http://nominatim.openstreetmap.org/search?format=xml&q=',$place_id))/searchresults/place[1]/@lat"/> 
                        </coordinates>
                    </Point>
-->
                </Placemark>
            </xsl:if>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
