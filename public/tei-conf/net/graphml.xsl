<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei xs"
    version="2.0">
    <xsl:template match="/">
        <xsl:result-document href="c07.graphml" indent="yes">
            <graphml>
                <key attr.name="type" attr.type="string" for="node" id="type"/>
                <key attr.name="r" attr.type="int" for="node" id="r"/>
                <key attr.name="g" attr.type="int" for="node" id="g"/>
                <key attr.name="b" attr.type="int" for="node" id="b"/>
                <graph edgedefault="undirected">
                    <xsl:apply-templates select="//tei:sourceDoc//tei:rs"/>
                </graph>
            </graphml>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="tei:rs">
        <xsl:if test="not(./@prev)">
            <xsl:choose>
                <xsl:when test="not(contains(./@ref, ' '))">
                    <xsl:variable select="./@ref" name="id"/>
                    <xsl:if test="not(preceding::*[@ref = $id or contains(@ref,concat(' ',$id)) or contains(@ref,concat($id,' '))])">
                        <node id="{substring(./@ref,2)}">
                            <xsl:choose>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring(current()/@ref,2)]) eq 'place'">
                                    <data key="type">place</data>
                                    <data key="r">191</data>
                                    <data key="g">1</data>
                                    <data key="b">245</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring(current()/@ref,2)]) eq 'person'">
                                    <data key="type">person</data>
                                    <data key="r">91</data>
                                    <data key="g">194</data>
                                    <data key="b">245</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring(current()/@ref,2)]) eq 'org'">
                                    <data key="type">org</data>
                                    <data key="r">245</data>
                                    <data key="g">91</data>
                                    <data key="b">91</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring(current()/@ref,2)]) eq 'event'">
                                    <data key="type">event</data>
                                    <data key="r">245</data>
                                    <data key="g">151</data>
                                    <data key="b">1</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring(current()/@ref,2)]) eq 'item'">
                                    <data key="type">work</data>
                                    <data key="r">91</data>
                                    <data key="g">245</data>
                                    <data key="b">91</data></xsl:when>
                            </xsl:choose>
                        </node>  
                    </xsl:if>
                    <xsl:variable name="refval" select="current()/@ref"/>
                    <xsl:for-each select="preceding::tei:rs[ancestor::tei:surface/@n eq current()/ancestor::tei:surface/@n][not(./@prev)]">                
                        <xsl:choose>
                            <xsl:when test="not(contains(./@ref, ' '))">
                                <edge source="{substring(./@ref,2)}" target="{substring($refval,2)}"/>        
                            </xsl:when>
                            <xsl:otherwise>
                                <edge source="{substring((substring-before(./@ref,' ')),2)}" target="{substring($refval,2)}"/>
                                <edge source="{substring((substring-after(./@ref,' ')),2)}" target="{substring($refval,2)}"/>
                            </xsl:otherwise>
                        </xsl:choose>                        
                    </xsl:for-each>        
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="refval1" select="substring-before(./@ref,' ')"/>
                    <xsl:variable name="refval2" select="substring-after(./@ref,' ')"/>
                    <xsl:if test="not(preceding::*[contains(@ref,concat(' ',$refval1)) or contains(@ref,concat($refval1,' '))])">
                        <node id="{substring($refval1,2)}">
                            <xsl:choose>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-before(current()/@ref,' ')),2)]) eq 'place'">
                                    <data key="type">place</data>
                                    <data key="r">191</data>
                                    <data key="g">1</data>
                                    <data key="b">245</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-before(current()/@ref,' ')),2)]) eq 'person'">
                                    <data key="type">person</data>
                                    <data key="r">91</data>
                                    <data key="g">194</data>
                                    <data key="b">245</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-before(current()/@ref,' ')),2)]) eq 'org'">
                                    <data key="type">org</data>
                                    <data key="r">245</data>
                                    <data key="g">91</data>
                                    <data key="b">91</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-before(current()/@ref,' ')),2)]) eq 'event'">
                                    <data key="type">event</data>
                                    <data key="r">245</data>
                                    <data key="g">151</data>
                                    <data key="b">1</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-before(current()/@ref,' ')),2)]) eq 'item'">
                                    <data key="type">work</data>
                                    <data key="r">91</data>
                                    <data key="g">245</data>
                                    <data key="b">91</data></xsl:when>
                            </xsl:choose>
                        </node>
                    </xsl:if>
                    <xsl:for-each select="preceding::tei:rs[ancestor::tei:surface/@n eq current()/ancestor::tei:surface/@n][not(./@prev)]">                
                        <xsl:choose>
                            <xsl:when test="not(contains(./@ref, ' '))">
                                <edge source="{substring(./@ref,2)}" target="{substring($refval1,2)}"/>        
                            </xsl:when>
                            <xsl:otherwise>
                                <edge source="{substring((substring-before(./@ref,' ')),2)}" target="{substring($refval1,2)}"/>
                                <edge source="{substring((substring-after(./@ref,' ')),2)}" target="{substring($refval1,2)}"/>
                            </xsl:otherwise>
                        </xsl:choose>                        
                    </xsl:for-each>
                    <xsl:if test="not(preceding::*[contains(@ref,concat(' ',$refval2)) or contains(@ref,concat($refval2,' '))])">                    
                        <node id="{substring($refval2,2)}">
                            <xsl:choose>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-after(current()/@ref,' ')),2)]) eq 'place'">
                                    <data key="type">place</data>
                                    <data key="r">191</data>
                                    <data key="g">1</data>
                                    <data key="b">245</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-after(current()/@ref,' ')),2)]) eq 'person'">
                                    <data key="type">person</data>
                                    <data key="r">91</data>
                                    <data key="g">194</data>
                                    <data key="b">245</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-after(current()/@ref,' ')),2)]) eq 'org'">
                                    <data key="type">org</data>
                                    <data key="r">245</data>
                                    <data key="g">91</data>
                                    <data key="b">91</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-after(current()/@ref,' ')),2)]) eq 'event'">
                                    <data key="type">event</data>
                                    <data key="r">245</data>
                                    <data key="g">151</data>
                                    <data key="b">1</data></xsl:when>
                                <xsl:when test="name(//tei:sourceDesc//*[@xml:id eq substring((substring-after(current()/@ref,' ')),2)]) eq 'item'">
                                    <data key="type">work</data>
                                    <data key="r">91</data>
                                    <data key="g">245</data>
                                    <data key="b">91</data></xsl:when>
                            </xsl:choose>
                        </node>
                    </xsl:if>
                    <xsl:for-each select="preceding::tei:rs[ancestor::tei:surface/@n eq current()/ancestor::tei:surface/@n][not(./@prev)]">                
                        <xsl:choose>
                            <xsl:when test="not(contains(./@ref, ' '))">
                                <edge source="{substring(./@ref,2)}" target="{substring($refval2,2)}"/>        
                            </xsl:when>
                            <xsl:otherwise>
                                <edge source="{substring((substring-before(./@ref,' ')),2)}" target="{substring($refval2,2)}"/>
                                <edge source="{substring((substring-after(./@ref,' ')),2)}" target="{substring($refval2,2)}"/>
                            </xsl:otherwise>
                        </xsl:choose>
                        
                    </xsl:for-each>                    
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
