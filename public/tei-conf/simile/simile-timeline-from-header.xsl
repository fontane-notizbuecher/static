<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:gnd="http://d-nb.info/standards/elementset/gnd#"
    exclude-result-prefixes="xs tei gnd"
    version="2.0">
    
    <xsl:template match="/">
        <xsl:result-document href="c07-simile-timeline.html" method="html">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />        
                    <script src="http://api.simile-widgets.org/timeline/2.3.1/timeline-api.js?bundle=true" type="text/javascript"></script>
                    <script>
                        function onLoad() {
                        var eventSource = new Timeline.DefaultEventSource();
                        var bandInfos = [
                        Timeline.createBandInfo({
                        eventSource:    eventSource,
                        date:           "1500",
                        width:          "70%", 
                        intervalUnit:   Timeline.DateTime.DECADE, 
                        intervalPixels: 30
                        }),
                        Timeline.createBandInfo({
                        overview:       true,
                        eventSource:    eventSource,
                        date:           "1500",
                        width:          "30%", 
                        intervalUnit:   Timeline.DateTime.CENTURY, 
                        intervalPixels: 60
                        })
                        ];
                        bandInfos[1].syncWith = 0;
                        bandInfos[1].highlight = true;
                        
                        tl = Timeline.create(document.getElementById("my-timeline"), bandInfos);
                        Timeline.loadXML("c07-simile-timeline.xml", function(xml, url) { eventSource.loadXML(xml, url); });
                        }
                    </script>        
                </head>
                <body onload="onLoad();" onresize="onResize();">
                    <div id="my-timeline" style="height: 500px; border: 1px solid #aaa"></div>
                    <noscript>This page uses Javascript to show you a Timeline. Please enable Javascript in your browser to see the full page. Thank you.</noscript>
                </body>
            </html>
        </xsl:result-document>        
        <xsl:result-document href="c07-simile-timeline.xml" method="xml" indent="yes">
            <data>
                <xsl:apply-templates select="//tei:person"/>
            </data>
        </xsl:result-document>        
    </xsl:template>
    
    <xsl:template match="tei:person"> 
        <xsl:if test="./tei:idno[@type eq 'GND']">
            <xsl:variable name="gndid" select="./tei:idno[@type eq 'GND']"/>
            <xsl:if test="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfBirth or document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfDeath">
                <event>
                    <xsl:attribute name="durationEvent">true</xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:preferredNameForThePerson"/>
                    </xsl:attribute>
                    <xsl:choose>
                        <xsl:when test="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfBirth">
                            <xsl:attribute name="start">
                                <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfBirth"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfDeath">
                            <xsl:attribute name="start">
                                <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfDeath - 100"/>
                            </xsl:attribute>
                            <xsl:attribute name="latestStart">
                                <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfDeath - 50"/>
                            </xsl:attribute>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfDeath">
                            <xsl:attribute name="end">
                                <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfDeath"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfBirth">
                            <xsl:attribute name="earliestEnd">
                                <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfBirth + 50"/>
                            </xsl:attribute>
                            <xsl:attribute name="end">
                                <xsl:value-of select="document(concat('http://d-nb.info/',$gndid,'/about/rdf'))//gnd:dateOfBirth + 100"/>
                            </xsl:attribute>
                        </xsl:when>
                    </xsl:choose>                            
                </event>   
            </xsl:if>
        </xsl:if>  
    </xsl:template>
    
</xsl:stylesheet>